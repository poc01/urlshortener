# URLShortener

Url shortener is a demo application that, given an arbitrary long URL creates a shortened version. You own the domain sho.com and you want to offer the URL shortening service. For example  http://website.com/looooooooooooong can be represented as  http://sho.com/a1b2

## Features
- Support a basic SEO url shortener based on specified keyword
- Support a generic url shortener with random 6-char alaphanumeric url substitution.
- Retrieve original URL given a SEO/Random shortened URL
- Collision Preventing


The demo application has been implemented using only core java ([JDK 1.8]()). Test cases are based on [Junit]() and project is based on [Maven Quickstart Archetype](https://maven.apache.org/archetypes/maven-archetype-quickstart/)

## Setup

URLShortner requires [git](), [jdk1.8]() and [maven]()

Clone git project
```sh
git clone git@bitbucket.org:poc01/urlshortener.git
cd urlshortener
```

Build application
```sh
mvn clean package -DskipTests
```
 
Run test cases
```sh
mvn test
```