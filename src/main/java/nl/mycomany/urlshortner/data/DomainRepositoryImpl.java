package nl.mycomany.urlshortner.data;

import static nl.mycomany.urlshortner.data.DomainRepositoryException.ERROR_CODE_DUPLICATE_KEYWORD;
import static nl.mycomany.urlshortner.data.DomainRepositoryException.ERROR_CODE_DUPLICATE_URL;

import java.util.Optional;

import nl.mycomany.urlshortner.data.MyDataBase.SearchCriteria;


/**
 * Implementation of {@link DomainRepository} interface
 * @author Ranjit
 *
 */

public class DomainRepositoryImpl implements DomainRepository{
	
	private final MyDataBase database;

	public DomainRepositoryImpl() {	
		this.database = new MyDataBase();
	}

	
	
	@Override
	public DomainRecord createShortUrl(String url, Optional<String> keyword_, Optional<Long> expireAt_) throws DomainRepositoryException {
		synchronized (url) {			
				String keyword = keyword_.orElse(null);
				
				long expireAt = expireAt_.orElse(-1L);
				
				SearchCriteria criteria = entity -> entity.getUrl().equalsIgnoreCase(url);
				
				DomainRecord record = database.getUrlEntity(criteria);
				
				if(record != null) {
				     throw new 	DomainRepositoryException(ERROR_CODE_DUPLICATE_URL, "Specified url already registered in the system");
				}
				
				if(keyword != null){
					criteria = entity -> keyword.equalsIgnoreCase(entity.getKeyword());
					record = database.getUrlEntity( criteria );
					if(record != null) {
					     throw new 	DomainRepositoryException(ERROR_CODE_DUPLICATE_KEYWORD, "Specified keyword already registered in the system with another url");
					}
				}
				
				record = new DomainRecord(url);
				record.setKeyword(keyword);
				record.setExpireAt(expireAt);				
				database.saveUrlEntity(record);
				
				return record;			
		}
		
	}

	@Override
	public DomainRecord retrieveUrlById(String id) throws DomainRepositoryException {
		
	   SearchCriteria criteria = entity -> entity.getId().equalsIgnoreCase(id) ;
		
	   DomainRecord record =  database.getUrlEntity(criteria);
	 	   
	   return record;
	}
	
	
	@Override
	public DomainRecord retrieveUrlByKeyword(String keyword) throws DomainRepositoryException {
		
	   SearchCriteria criteria = entity -> keyword.equalsIgnoreCase(entity.getKeyword());
		
	   DomainRecord record =  database.getUrlEntity(criteria);
	 	   
	   return record;
	}



	@Override
	public DomainRecord retrieveUrlByIdOrKeyword(String keywordOrId) throws DomainRepositoryException {
		   SearchCriteria criteria = entity -> entity.getId().equalsIgnoreCase(keywordOrId)  || keywordOrId.equalsIgnoreCase(entity.getKeyword());
			
		   DomainRecord record =  database.getUrlEntity(criteria);
		 	   
		   return record;
	}
	
	
	
	
	
	

}
