package nl.mycomany.urlshortner.data;

import java.util.Optional;

/**
 * Data layer Interface class in order to enable interactions with Database
 * 
 * @author Ranjit
 *
 */
public interface DomainRepository {

	/**
	 * Create a new URL mapping into the system in order to facilitate it's shortening service.
	 * 
	 * @param Uri
	 * @param keyword
	 * @param expireAt
	 * @return  newly created DomainRecord
	 * @throws DomainRepositoryException
	 */
	DomainRecord createShortUrl(String Uri, Optional<String> keyword, Optional<Long> expireAt) throws DomainRepositoryException;

	/**
	 * Retrieve third party original url related to specified identifier (short string)  
	 * 
	 * @param id Identifier
	 * @return DomainRecord matching with specified identifier
	 * @throws DomainRepositoryException
	 */
	DomainRecord retrieveUrlById(String id) throws DomainRepositoryException;
	
	/**
	 * Retrieve third party original url related to specified keyword
	 * 
	 * @param keyword
	 * @return DomainRecord matching with specified keyword
	 * @throws DomainRepositoryException
	 */
	DomainRecord retrieveUrlByKeyword(String keyword) throws DomainRepositoryException;
	
	/**
	 * Retrieve third party original url related to specified input which is either keyword or  identifier
	 *  
	 * @param keywordOrId
	 * @return
	 * @throws DomainRepositoryException
	 */
	DomainRecord retrieveUrlByIdOrKeyword(String keywordOrId) throws DomainRepositoryException;
	
	
}
