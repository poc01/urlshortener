package nl.mycomany.urlshortner.data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 	
 * Class to represent a  'SIMULATED DATABASE'
 * 
 * @author Ranjit 
 *
 */

public class MyDataBase {

    // Cycle period of cleanup cleanup thread responsible for remove expired URL entries from from data store 	
	public static final int CLEANUP_CYCLE_DURATION =  1000 * 60 * 60 * 60 ;// 60 minutes
	
	public static final int SEQ_NUM_RADIX = 36;
	// current index value of short url identifier represented in 6-char 36-base number. This value keeps on incrementing from default value every
	// time a new URL registered into the system
	private static long currentSequecnce =   Long.valueOf("f0eydx", SEQ_NUM_RADIX);
	
	//In memory data store in form of Array List
	private List<DomainRecord> domainRegister = new ArrayList<>(50);
	
	//This object is used by maintenance thread for locking purpose 
	private Object maintanceLock = new Object();
	
	public MyDataBase() {
		startMaintainance();
	}

	/*
	 * Save new url into store
	 * @param entity
	 * @return Short Identifier
	 */
	public String saveUrlEntity(DomainRecord entity) {		
		String nextSeq = getNextSequence();
		entity.setId(nextSeq);
		domainRegister.add(entity);
		return nextSeq;
	}
	
	/*
	 * Search URL record based on specified criteria. This method always make sure that it does not return expired record.
	 * 
	 * @param criteria
	 * @return DomainRecord
	 */
	public DomainRecord getUrlEntity(SearchCriteria criteria) {	
		long currentTime = System.currentTimeMillis();
		List<DomainRecord> matchedRecords =  domainRegister.stream().filter(entity->{
			return (entity.getExpireAt() < 0|| currentTime < entity.getExpireAt()) && criteria.evaluate(entity);
			
		}).collect(Collectors.toList());
		return matchedRecords.size() > 0 ? matchedRecords.get(0) : null; 
	}
	
	/**
	 * This method generate unique 6-char alphanumeric identifier om each request.
	 * Thread safety has been insured to avoid read/write anomalies related to concurrency 
	 * 
	 * @return
	 */
	private synchronized static String getNextSequence() {
		currentSequecnce++;
		String nextSeq = Long.toString(currentSequecnce, SEQ_NUM_RADIX).toUpperCase();
		return nextSeq;
	}
	
	
	/**
	 * This method starts a maintenance thread to periodically clean expired entries.  
	 */
	private void startMaintainance() {		
		Runnable maintainaceActivity = () -> {
			System.out.println("Starting cleanup thread");
			while(true) {
				
				synchronized (maintanceLock) {
					try {					
						maintanceLock.wait(CLEANUP_CYCLE_DURATION);
						
						long currentTime = System.currentTimeMillis();
						
						List<DomainRecord> expiredEntries = domainRegister.stream().filter(entry -> entry.getExpireAt() >= currentTime).collect(Collectors.toList());
						
						if(expiredEntries.size() >0) {
							System.out.println(String.format("Clearing %d expired entries from url register", expiredEntries.size()));
							domainRegister.removeAll(expiredEntries);
						}
						
						maintanceLock.notifyAll();
					
					}catch (InterruptedException e) {
						System.err.println("Maintainance thread wait interrupted");
						e.printStackTrace();
					}				
					
				}
			
			}
		};
		
		Thread maintainance = new Thread(maintainaceActivity);
		maintainance.start();
		
	}
	
	
	
	/**
	 * a functional interface to construct a search criteria in order to fetch registered urls 
	 * 
	 * @author Ranjit
	 *
	 */
	static interface SearchCriteria{
		boolean evaluate(DomainRecord entity);
	}
	
	
	



}
