package nl.mycomany.urlshortner.data;

/**
 * An Exception class to represent data layer errors.
 * @author Ranjit
 *
 */
public class DomainRepositoryException  extends Exception {
	
	private static final long serialVersionUID = 1L;

	public static final int ERROR_CODE_DUPLICATE_URL = 100;
	
	public static final int ERROR_CODE_DUPLICATE_KEYWORD = 101;
	
	public static final int ERROR_CODE_OTHER = 102;
	
	private final int errorCode;

	public DomainRepositoryException(int errorCode, String msg) {
		super(msg);
		this.errorCode = errorCode;
	}
	
	public DomainRepositoryException(int error_code, Throwable t) {
		super(t);
		this.errorCode = error_code;
	}

	public int getErrorCode() {
		return errorCode;
	}

	 
	

}
