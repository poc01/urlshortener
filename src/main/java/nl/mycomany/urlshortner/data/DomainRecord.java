package nl.mycomany.urlshortner.data;

import java.io.Serializable;

/**
 * A domain class to represent a third party URL, associated short version, keyword and expiration period.  
 * 
 * @author Ranjit
 *
 */
public class DomainRecord implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/*An auto generated fixed length alpha-numeric identifier to map specific third party urls, also used to construct short version*/
	private String id;
	
	/*NULLABLE, applicable when keyword is used for short url version  */
	private String keyword;
	
	/* Original thrid party URL  */
	private String Url;
	
	/* Expiration Time in milliseconds in order to hide this record from customers, -ve value mean this will never expire  */
	private long expireAt = -1;

	public DomainRecord() {
		
	}
	
	

	public DomainRecord(String url) {
		super();
		Url = url;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

	public long getExpireAt() {
		return expireAt;
	}

	public void setExpireAt(long expireAt) {
		this.expireAt = expireAt;
	}

		 
	
 	

}
