package nl.mycomany.urlshortner.service;

/**
 * An Exception class to represent service layer errors.
 * 
 * @author Ranjit
 *
 */
public class DomainServiceException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public static final int ERROR_CODE_DUPLICATE_URL = 200;
	
	public static final int ERROR_CODE_DUPLICATE_KEYWORD = 201;
	
	public static final int ERROR_CODE_URL_LENGTH_TOO_BIG = 202;
	
	public static final int ERROR_CODE_INVALID_URL_FORMAT = 203;
	
	public static final int ERROR_CODE_INVALID_EXP_DATE = 204;
	
	public static final int ERROR_CODE_KEYWORD_TOO_BIG = 205;
	
	public static final int ERROR_CODE_OTHER = 206;
	
	private final int error_code;

	public DomainServiceException(int error_code, String msg) {
		super(msg);
		this.error_code = error_code;
	}
	
	public DomainServiceException(int error_code, Throwable t) {
		super(t);
		this.error_code = error_code;
	}

	public int getError_code() {
		return error_code;
	}
	
	

}
