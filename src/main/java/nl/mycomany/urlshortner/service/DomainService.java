package nl.mycomany.urlshortner.service;

import java.util.Optional;

import nl.mycomany.urlshortner.data.DomainRepositoryException;

/**
 * 
 * Service layer interface related to URL shortening functionalities. 
 * 
 * @author Ranjit
 *
 */
public interface DomainService {
	
	/**
	 * Returns base url of URL Shortening Service
	 * 
	 * @return based url
	 */
	String getBaseUrl();
	
	/**
	 * Register a new url and returns it's short identifier. 
	 * 
	 * @param url Original URL
	 * @param keyword Keyword
	 * @param expireAt expire time
	 * @return short identifier.
	 * @throws DomainServiceException specified url is Invalid, blank, too large or duplicate
	 */
	String createShortUrl(String url, Optional<String> keyword, Optional<Long> expireAt) throws DomainServiceException;
	
	
	/**
	 * Retrieve third party original url based on specified shortURL
	 * @param shorUrl
	 * @return
	 * @throws DomainServiceException
	 */
	String retrieveUrl(String shortUrl) throws DomainServiceException;

	
	/**
	 * Retrieve third party original url related to specified identifier (short string)  
	 * 
	 * @param id Identifier
	 * @return DomainRecord matching with specified identifier
	 * @throws DomainRepositoryException
	 */
	String retrieveUrlById(String id) throws DomainServiceException;
	
	/**
	 * Retrieve third party original url related to specified keyword
	 * 
	 * @param keyword
	 * @return DomainRecord matching with specified keyword
	 * @throws DomainRepositoryException
	 */
	String retrieveUrlByKeyword(String keyword) throws DomainServiceException;
	
	/**
	 * Retrieve third party original url related to specified input which is either keyword or  identifier
	 *  
	 * @param keywordOrId
	 * @return
	 * @throws DomainRepositoryException
	 */
	String retrieveUrlByIdOrKeyword(String keywordOrId) throws DomainServiceException;
		
}
