package nl.mycomany.urlshortner.service;

import nl.mycomany.urlshortner.data.DomainRepository;
import nl.mycomany.urlshortner.data.DomainRepositoryImpl;

/**
 * Factory class based on Singleton Factory Pattern to create URL Shortening Service Object
 * @author Ranjit
 *
 */
public class DomainServiceFactory {
	
	public static final String BASE_URL = "http://sho.com"; 
			
	
	private static DomainService service;
	
	
	static {
		DomainRepository repository = new DomainRepositoryImpl();
		
		service = new DomainServiceImpl(BASE_URL, repository);
	}
	
	
	public static DomainService getDomainService() {
		return service;
		
	}
}
