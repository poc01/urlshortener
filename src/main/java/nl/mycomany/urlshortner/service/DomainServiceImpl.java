package nl.mycomany.urlshortner.service;

import static nl.mycomany.urlshortner.service.DomainServiceException.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import nl.mycomany.urlshortner.data.DomainRecord;
import nl.mycomany.urlshortner.data.DomainRepository;
import nl.mycomany.urlshortner.data.DomainRepositoryException;

/**
 * Implementation class of {@link DomainService}
 * 
 * @author Ranjit
 *
 */
public class DomainServiceImpl implements DomainService{
    
	private static final int URL_MAX_LENGTH = 200;
	
	private static final int KEYWORD_MAX_LENGTH = 12;
	
	private final String baseUrl;
	
	private final DomainRepository repository;
	
	public DomainServiceImpl(String baseUrl, DomainRepository repository) {
		super();
		this.baseUrl = baseUrl;
		this.repository = repository;
	}
	
	@Override
	public String getBaseUrl() {
		return baseUrl;
	}

	@Override
	public String createShortUrl(String url, Optional<String> keyword, Optional<Long> expireAt)throws DomainServiceException {
		
		if(url == null || url.trim().length()==0) {
			throw new DomainServiceException(ERROR_CODE_OTHER, "Url can not be null or blank");
		}
		
		if(url.length() > URL_MAX_LENGTH) {
			throw new DomainServiceException(ERROR_CODE_URL_LENGTH_TOO_BIG, "Url length can not be more than:"+URL_MAX_LENGTH+" characters");
		}
		
		
		try {
			 new URL(url);
		} catch (MalformedURLException e) {
			throw new DomainServiceException(ERROR_CODE_INVALID_URL_FORMAT, e);
		}
		
		if(keyword.isPresent() && keyword.get().trim().length()==0) {
			throw new DomainServiceException(ERROR_CODE_OTHER, "Keword must be null or blank");
		}
		
		if(keyword.isPresent() && keyword.get().length() > KEYWORD_MAX_LENGTH) {
			throw new DomainServiceException(ERROR_CODE_KEYWORD_TOO_BIG, "Keword length can not be more than:"+KEYWORD_MAX_LENGTH+" characters");
		}
		
		if(expireAt.isPresent() && expireAt.get() < System.currentTimeMillis()) {
			throw new DomainServiceException(ERROR_CODE_INVALID_EXP_DATE, "Expire At must be in future");
		}
		
		
		try {
			
			DomainRecord record =  repository.createShortUrl(url, keyword, expireAt);
			return this.baseUrl+"/"+record.getId();
			
		} catch (DomainRepositoryException e) {
            if(e.getErrorCode() == DomainRepositoryException.ERROR_CODE_DUPLICATE_KEYWORD) {
            	throw new DomainServiceException(ERROR_CODE_DUPLICATE_KEYWORD, "Duplicate Keyword"+ keyword);
            }
            else if(e.getErrorCode() == DomainRepositoryException.ERROR_CODE_DUPLICATE_URL) {
            	throw new DomainServiceException(ERROR_CODE_DUPLICATE_URL, "Duplicate Url:"+url);
            }			
            else {
            	throw new DomainServiceException(ERROR_CODE_OTHER, e.getMessage());
            }
			
		}		
		  
	
	}
	
	

	@Override
	public String retrieveUrl(String shortUrl) throws DomainServiceException {
		
		if(shortUrl == null || shortUrl.trim().length()==0) {
			throw new DomainServiceException(ERROR_CODE_OTHER, "shortUrl can not be null or blank");
		}
		
		try {
			 new URL(shortUrl);
		} catch (MalformedURLException e) {
			throw new DomainServiceException(ERROR_CODE_INVALID_URL_FORMAT, e);
		}
		
		
		String shortUrlIdOrKeyword = shortUrl.substring(shortUrl.lastIndexOf("/")+1);
		return retrieveUrlByIdOrKeyword(shortUrlIdOrKeyword);
	}

	@Override
	public String retrieveUrlById(String id) throws DomainServiceException {
		
		try {
			DomainRecord record = repository.retrieveUrlById(id);
			if(record != null) {
				return record.getUrl();
			}
		}catch(DomainRepositoryException e) {
			throw new DomainServiceException(ERROR_CODE_OTHER, e.getMessage());
		}		
		return null;
	}

	@Override
	public String retrieveUrlByKeyword(String keyword) throws DomainServiceException {
		
		try {
			DomainRecord record = repository.retrieveUrlByKeyword(keyword);
			if(record != null) {
				return record.getUrl();
			}
		}catch(DomainRepositoryException e) {
			throw new DomainServiceException(ERROR_CODE_OTHER, e.getMessage());
		}
		return null;
	}

	@Override
	public String retrieveUrlByIdOrKeyword(String keywordOrId) throws DomainServiceException {
		
		try {
			DomainRecord record = repository.retrieveUrlByIdOrKeyword(keywordOrId);
			
			if(record != null) {
				return record.getUrl();
			}
			
		}catch(DomainRepositoryException e) {
			throw new DomainServiceException(ERROR_CODE_OTHER, e.getMessage());
		}
		
		return null;
	}
	
	
	
	

	
	
	
	
	
	

	
	
	

}
