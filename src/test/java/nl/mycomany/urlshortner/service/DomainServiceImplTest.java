package nl.mycomany.urlshortner.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;


public class DomainServiceImplTest {
	
    public static final int SHORT_URL_LENGTH = 6; 
	
	private DomainService service;
	
	@Before
	public void setup() {
		service = DomainServiceFactory.getDomainService();
	}
	

	@Test
	public void testCreateShortUrlByKeyword() throws DomainServiceException{		
		//Short url creation by keyword
		for(int i=1; i<=100; i++) {
			String keyword = "keyword"+i;
			String url = "http://mycomanyweb.com/"+keyword;
			String shortUrl = service.createShortUrl(url, Optional.of(keyword), Optional.ofNullable(null));
			System.out.println("Short Url:"+shortUrl+" Target Url:"+url);
			
			String retrievedUrl = service.retrieveUrlByKeyword(keyword);
			assertEquals(retrievedUrl, url);
		}
		
	}	
	
	@Test
	public void testCreateShortUrl() throws DomainServiceException{
		
		//Short url creation based on random 6-char alphanumeric identifier
		
		for(int i=1; i<=100; i++) {
			String resource = "resource"+i;
			String url = "http://mycomanyweb.com/abcd/xyz/pqr/"+resource;
			String shortUrl = service.createShortUrl(url, Optional.ofNullable(null), Optional.ofNullable(null));			
			
			
			String shortUrlName = shortUrl.substring(shortUrl.lastIndexOf("/")+1);
			
			//Validating Length
			assertEquals(shortUrlName.length(), SHORT_URL_LENGTH);  
			
			//Validating short url name using regex representing alpha numeric chars 
			assertTrue(shortUrlName.matches("^[a-zA-Z0-9]*$")); //short url name is only alpha numeric			
			
			System.out.println("Short Url:"+shortUrl+" Target Url:"+url);
			
			 
			String retrievedUrl = service.retrieveUrlById(shortUrlName);
			assertEquals(retrievedUrl, url);
						
		}		
	}
	
	@Test
	public void testRetrieveUrl() throws DomainServiceException{
		
		//retrieve url buy full short url, id or keyword
		String url = "http://mycomanyweb.com/abcd/xyz/pqr/123234";
		String shortUrl = service.createShortUrl(url, Optional.ofNullable(null), Optional.ofNullable(null));
		String urlId = shortUrl.substring(shortUrl.lastIndexOf("/")+1);
		
		
		//Retrieve by shortUrl
		String retrievedUrl = service.retrieveUrl(shortUrl);
		assertEquals(retrievedUrl, url);
				
		//Retrieve by url id
		retrievedUrl = service.retrieveUrlById(urlId);
		assertEquals(retrievedUrl, url);		
	}
	
	@Test
	public void testRetrieveKeywordUrl() throws DomainServiceException{
		
		//retrieve url buy full short url, id or keyword
		String url = "http://mycomanyweb.com/abcd/xyz/pqr/abcd2";
		String keyword = "myKeyword";
		String shortUrl = service.createShortUrl(url, Optional.of(keyword), Optional.ofNullable(null));
		String urlId = shortUrl.substring(shortUrl.lastIndexOf("/")+1);
		
		//Retrieve by keyword
		String retrievedUrl = service.retrieveUrlByKeyword(keyword);
		assertEquals(retrievedUrl, url);
		
		
		//Retrieve by shortUrl
		retrievedUrl = service.retrieveUrl(shortUrl);
		assertEquals(retrievedUrl, url);
				
		//Retrieve by url id
		retrievedUrl = service.retrieveUrlById(urlId);
		assertEquals(retrievedUrl, url);		
	}
	
	
	
	@Test
	public void testTimeToLive() throws Exception{		
		//Testing time to live scenario
		String url = "http://mycomanyweb.com/frty/ddf/xyzqweqwer";
		long timeToLive = System.currentTimeMillis() + 2000; //2 seconds
		
		//Creating short url with 2-second time to live
		String shortUrl = service.createShortUrl(url, Optional.ofNullable(null), Optional.of(timeToLive));		 
		String retrievedUrl = service.retrieveUrl(shortUrl);
		
		assertEquals(retrievedUrl, url);
		
		//wait for 2-seconds and make sure url has GONE
		Thread.sleep(2000);
		retrievedUrl = service.retrieveUrl(shortUrl);
		assertNull(retrievedUrl);
	}
	
	/////////////////////Negative test cases///////////////////// 
	
	@Test(expected = DomainServiceException.class)
	public void testCreateShortUrlByDuplicateKeyword() throws DomainServiceException{
		//Duplicate keword is not allowed
		String keyword = "abcdefed";
		String url = "http://mycomanyweb.com/abcdefed";
		
		service.createShortUrl(url, Optional.of(keyword), Optional.ofNullable(null));
		
		service.createShortUrl(url, Optional.of(keyword), Optional.ofNullable(null));
		
	}
	
	@Test(expected = DomainServiceException.class)
	public void testCreateShortUrlByDuplicateUrl() throws DomainServiceException{		
		//Duplicate URL is not allowed
		String url = "http://mycomanyweb.com/xyzqwe";
		
		service.createShortUrl(url, Optional.ofNullable(null), Optional.ofNullable(null));
		
		service.createShortUrl(url, Optional.ofNullable(null), Optional.ofNullable(null));
	}
	
	
	@Test(expected = DomainServiceException.class)
	public void testKeywordToLong() throws DomainServiceException{
		//Keyword is more than required length
		String keyword = "abcdruioewuroieuwoiruewoiruioewurioewu";
		String url = "http://mycomanyweb.com/abcde";
		service.createShortUrl(url, Optional.of(keyword), Optional.ofNullable(null));
		
	}
	
	@Test(expected = DomainServiceException.class)
	public void testKeywordUrlTooLong() throws DomainServiceException{
		//URL is more than required length
		String url = "http://mycomanyweb.com/abcde44444444444444444444sadsad/sdsadsddddddddddddddddddddddd/3434/dfdsfffffffffffffffffffffffffffffff/dfffffffffffffffffff/5555555555555555555555555/dffffrrrrrrrrrrrrrrrrrrrrrrr/ddddddddddddddddddd/ddddddddddddddddd";
		service.createShortUrl(url, Optional.ofNullable(null), Optional.ofNullable(null));
		
	}	
	
	
	
	
	
	
	
	
	
	
	

}
